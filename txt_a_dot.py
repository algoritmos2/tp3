
import sys

def txt_to_dot(nombre_archivo, nombre_salida):

	with open(nombre_archivo,"r") as f, open(nombre_salida,"w") as salida:

		cant_vertices = f.readline()
		cant_vertices = int(cant_vertices)

		for i in xrange(cant_vertices):
			linea = f.readline() #temp

		cant_aristas = int(f.readline())

		salida.write("Graph G {\n")
		salida.write("	overlap = scale;\n")
		salida.write("	splines = true;\n")

		for i in xrange(cant_aristas):

			linea = f.readline()
			linea = linea.rstrip("\n")
			linea = linea.lstrip()
			linea = linea.rstrip()

			args = linea.split()

			salida.write("	%s -- %s;\n" % (args[1], args[2]))

		salida.write("}\n")

"""USO:
	arg1: nombre archivo de entrada
	arg2: nombre archivo de salida
"""
def main():
	txt_to_dot(sys.argv[1], sys.argv[2]);

main()