
import sys


def listas_de_adyacencia(nombre_archivo):

	d = {}
	with open(nombre_archivo,"r") as f:
		cant_vertices = int(f.readline())

		for i in xrange(cant_vertices):
			linea = f.readline()
			linea = linea.rstrip("\n")
			linea = linea.lstrip()
			linea = linea.rstrip()
			args = linea.split()
			d[args[1]] = []

		cant_aristas = int(f.readline())
		for i in xrange(cant_aristas):

			linea = f.readline()
			linea = linea.rstrip("\n")
			linea = linea.lstrip()
			linea = linea.rstrip()
			args = linea.split()

			d[args[1]].append(args[2])

	return d


def crear_subgrafo(listas_de_adyacencia, nombre_vertice):

	subgrafo = {} #otra lista de adyacencia

	a_visitar = [nombre_vertice]

	while a_visitar:

		for vertice in list(a_visitar):

			print vertice
			subgrafo[vertice] = listas_de_adyacencia.pop(vertice)

			for adyacente in subgrafo[vertice]:

				if adyacente in listas_de_adyacencia:
					if adyacente not in a_visitar:
						a_visitar.append(adyacente)

			a_visitar.remove(vertice)

		if not a_visitar: #a_visitar esta vacio

			for vertice in listas_de_adyacencia:
				for adyacente in listas_de_adyacencia[vertice]:
					if adyacente in subgrafo:
						if vertice not in a_visitar:
							a_visitar.append(vertice)

	return subgrafo

def subgrafo_a_archivo(subgrafo, nombre_salida):

	with open(nombre_salida,"w") as salida:

		salida.write("%d\n" % len(subgrafo))

		i = 0
		suma_aristas = 0
		for vertice in subgrafo.iterkeys():
			salida.write("%d %s\n" % (i, vertice))
			i += 1
			suma_aristas += len(subgrafo[vertice])

		salida.write("%d\n" % suma_aristas)

		i = 0
		for vertice in subgrafo.iterkeys():
			for adyacente in subgrafo[vertice]:
				salida.write("%d %s %s\n" % (i, vertice, adyacente))
				i += 1

"""USO: 
	arg1: archivo_de_entrada
	arg2: nombre de un vertice
	arg3: nombre_archivo de salida

"""
def main():
	d = listas_de_adyacencia(sys.argv[1])
	print d
	subgrafo = crear_subgrafo(d, sys.argv[2])
	subgrafo_a_archivo(subgrafo, sys.argv[3])

main()